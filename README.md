# README #

WP Private Site is a plugin that allows you to block your website from any user that is not whitelisted.
It protects from access to any part of the site and forces any non whitelisted user to see a landing page.

### What is this repository for? ###

This repository is for keeping track of the changes made to the code of the WP Private Site wordpress plugin.

Version 1.0

### How do I get set up? ###

Set Up

To install this plugin just download the zip folder and upload it through the wordpress dashboard in the add new plugin screen. 
Then activate it and go to Settings -> Wordpress Private Site to setup.

Configuration

To grant a user access, just type the username in the input box and press the button 'Grant Access'. Once a user appears on the list, the site will display for him/her.
You need to mark the 'enabled' checkbox to activate the blocking.

Dependencies

The following libraries are required for the dashboard interface to work properly:

* jQuery
* jQueryUI Core
* jQueryUI Widget
* jQueryUI Autocomplete
* jQueryUI Tooltip
* jQueryUI Button

But the plugin is rigged to work with the precompiled version of those libraries that is included with wordpress.

### Who do I talk to? ###

wpdev@lobodeguerra.net